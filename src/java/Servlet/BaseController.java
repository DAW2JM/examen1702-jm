/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prueba
 */
abstract class BaseController extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(BaseController.class.getName());

    protected String contextPath;
    protected HttpServletRequest request;
    protected HttpServletResponse response;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.contextPath = getServletContext().getContextPath();

        this.request = request;
        this.response = response;

        String methodName = ((String) request.getAttribute(FrontController.METHOD_ATTRIBUTE)).toLowerCase();
        ArrayList<String> parameters = (ArrayList<String>) request.getAttribute(FrontController.PARAMETERS_ATTRIBUTE);

        try {
            Method method = getMethod(methodName);
            Class[] myMethodParamTypes = method.getParameterTypes();
            method.invoke(this, Arrays.copyOfRange(parameters.toArray(), 0, myMethodParamTypes.length));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/error/index.jsp");
            request.setAttribute("ex", ex);
            request.setAttribute("msg", "Error al procesar el método " + methodName + "->(" + String.join(", ", parameters) + ")");
            dispatcher.forward(request, response);
            return;

        }
    }

    private Method getMethod(String name) {
        Method[] methods = getClass().getMethods();
        Method method = null;

        for (Method m : methods) {
            if (m.getName().equals(name)) {
                method = m;
                break;
            }
        }

        return method;
    }

    protected void dispatch(String path) {
        RequestDispatcher dispatcher = request.getRequestDispatcher(path);

        try {
            dispatcher.forward(request, response);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    protected void redirect(String location) {
        
        try {
            LOG.info("Vamos a redirigir");
            response.sendRedirect(location);
            LOG.info("Redirigido");
            return;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    protected long toId(String value) {
        // Convert a string to a valid ID.
        // -1 == Error.
        // >0 == Valid.
        try {
            return Long.parseLong(value);
        } catch (Exception ex) {
            return -1;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
