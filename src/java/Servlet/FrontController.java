/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class FrontController extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(FrontController.class.getName());
    private static final String DEFAULT_CONTROLLER = "study";
    private static final String DEFAULT_METHOD = "index";

    public static final String METHOD_ATTRIBUTE = "method";
    public static final String PARAMETERS_ATTRIBUTE = "parameters";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOG.info("desde el front controller");

        try {
            String path = request.getPathInfo();
            path = path.substring(1);
            ArrayList<String> pathArray = new ArrayList<>(Arrays.asList(path.split("/")));

            String controller = controllerOrDefault(pathArray);
            String method = methodOrDefault(pathArray);

            RequestDispatcher dispatcher = getServletContext().getNamedDispatcher(controller);

            if (dispatcher != null) {
                request.setAttribute(METHOD_ATTRIBUTE, method);
                request.setAttribute(PARAMETERS_ATTRIBUTE, pathArray);

                dispatcher.forward(request, response);
                return;

            }

            // Si no reenviamos la petición al siguiente controlador...
            throw new Exception("Controlador " + controller + " no encontrado!!!");

        } catch (Exception ex) {
            LOG.info("Error al procesar el controlador !!!");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/error/index.jsp");
            request.setAttribute("ex", ex);
            request.setAttribute("msg", "Error al procesar el controlador ");
            dispatcher.forward(request, response);
            return;
        }

    }

    private String controllerOrDefault(ArrayList<String> pathArray) {
        String controller;
        if (pathArray.size() == 0) {
            controller = DEFAULT_CONTROLLER;
        } else {
            controller = pathArray.remove(0);
            controller = controller.equals("") ? DEFAULT_CONTROLLER : controller;
        }
        return controller.substring(0, 1).toUpperCase() + controller.substring(1) + "Controller";
    }

    private String methodOrDefault(ArrayList<String> pathArray) {
        String method;
        //method or default
        if (pathArray.size() == 0) {
            method = DEFAULT_METHOD;
        } else {
            method = pathArray.remove(0);
        }
        return method;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
