/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Post;
import persistence.PostDAO;

/**
 *
 * @author usuario
 */
public class PostController extends BaseController {

    private static final Logger LOG = Logger.getLogger(PostController.class.getName());
    private PostDAO postDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void index() {

        //objeto persistencia
        postDAO = new PostDAO();
        ArrayList<Post> posts = null;

        //leer datos de la persistencia
        synchronized (postDAO) {
            posts = postDAO.getAll();
        }
        request.setAttribute("posts", posts);
        String name = "index";
        

        dispatch("/WEB-INF/view/post/index.jsp");
        LOG.info("en index de post controller" + name);
    }

    public void create() {
        dispatch("/WEB-INF/view/post/create.jsp");
    }
    public void view(String idString) throws SQLException  {
        
        HttpSession sesion = request.getSession();
        sesion.setAttribute("ultimo_id", idString);
        
        long id = toId(idString);
        PostDAO  postDao = new PostDAO();
        Post post = postDao.get(id);
        request.setAttribute("post", post);
        dispatch("/WEB-INF/view/post/view.jsp");
        
        
    }
    public void last(String idString) throws SQLException  {
        
        long id = toId(idString);
        PostDAO  postDao = new PostDAO();
        Post post = postDao.get(id);
        request.setAttribute("post", post);
        dispatch("/WEB-INF/view/post/view.jsp");
    }
    
    
    
    public void insert() throws IOException {

        postDAO = new PostDAO();
        LOG.info("crear DAO");
        Post post = loadFromRequest();
        
        synchronized (postDAO) {
            try {
                postDAO.insert(post);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/post/index");
    }
    
    private Post loadFromRequest()
    {
        Post post = new Post();
        LOG.info("Crear modelo");
        post.setId(toId(request.getParameter("id")));
        post.setAutor(request.getParameter("autor"));
        post.setTitulo(request.getParameter("titulo"));
        post.setContent(request.getParameter("contenido"));
        LOG.info("Datos cargados");
        return post;
    }
}
