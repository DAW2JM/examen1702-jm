package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author usuario
 */
public class BaseDAO {
    private static final Logger LOG = Logger.getLogger(BaseDAO.class.getName());

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://192.168.92.132/examen1702";

//    public static final String DB_URL = "jdbc:mysql://10.0.2.6/tuition";
    public static final String DB_USER = "usuario";
    public static final String DB_PASS = "usuario";

    protected Connection connection;


    public void connect() {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            LOG.info("bien");
        } catch (ClassNotFoundException ex) {
            
        } catch (SQLException ex) {
           
        }
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            
        }
    }
}
