<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Post"%>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Alta de post</h1>
    <form action="<%= request.getContextPath()%>/post/insert" method="post">
        <label>Autor</label><input name="autor" type="text"><br>
        <label>Titulo</label><input name="titulo" type="text"><br>
        <label>Contenido</label><input name="contenido" type="text"><br>
        <label></label><input value="Enviar" type="submit"><br>
    </form>     
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
