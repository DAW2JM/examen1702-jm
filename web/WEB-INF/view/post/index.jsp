<%-- 
    Document   : index
    Created on : 27-feb-2017, 17:13:05
    Author     : usuario
--%>

<%@page import="model.Post"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="posts" class="java.util.ArrayList" scope="request"/>  

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>


<div id="content">
    <h1>Lista de Posts </h1>
    <p>
        <a href="<%= request.getContextPath()%>/post/create">Nuevo post</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>Autor</th>
            <th>Titulo</th>
            <th></th>
        </tr>
        <%        Iterator<model.Post> iterator = posts.iterator();
            while (iterator.hasNext()) {
                Post post = iterator.next();%>
        <tr>
            <td><%= post.getId()%></td>
            <td><%= post.getAutor()%></td>
            <td><%= post.getTitulo()%></td>
            <td> 
                <a href="<%= request.getContextPath() + "/post/view/" + post.getId()%>"> Ver</a>
            </td>
        </tr>
        <%
            }
        %>          </table>

</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
