<%-- 
    Document   : index
    Created on : 27-feb-2017, 17:13:05
    Author     : usuario
--%>

<%@page import="model.Post"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="post" class="model.Post" scope="request"/>  

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>


<div id="content">
    
    <h1><%= post.getTitulo()%></h1>

    <p><%= post.getContent()%></p>
    <label><%= post.getAutor()%></label>
    
    
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
